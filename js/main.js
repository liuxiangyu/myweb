(function () {
    // 获取窗口的高度和宽度
    var h = window.innerHeight;
    var w = window.innerWidth;

    // 获取所有类名为 "box_bg" 的元素
    var bg = document.getElementsByClassName("box_bg");


    // 根据窗口宽高比例调整背景元素的大小和位置
    if ((w / h) >= (1920 / 1080)) {
        for (var i = 0; i < bg.length; i++) {
            // 宽度设为窗口宽度，高度按照1920x1080的比例计算
            bg[i].style.width = w + 'px';
            bg[i].style.height = w * (1080 / 1920) + 'px';
            bg[i].style.top = -(w * (1080 / 1920) - h) / 2 + 'px';
            bg[i].style.left = '0';
        }
    }
    else {
        for (var i = 0; i < bg.length; i++) {
            // 高度设为窗口高度，宽度按照1920x1080的比例计算
            bg[i].style.height = h + 'px';
            bg[i].style.width = h * (1920 / 1080) + 'px';
            bg[i].style.left = -(h * (1920 / 1080) - w) / 2 + 'px';
            bg[i].style.top = '0';
        }
    }

    // 初始化 box01_index，用于控制文字逐个显示的效果
    var box01_index = 0;
    // 获取ID为 'box01_text' 的元素下的子元素集合
    var box01_p = document.getElementById('box01_text').children;
    // 定义 boxOne 函数，用于文字逐个显示效果的定时器回调
    function boxOne() {
        if (box01_index >= 0 && box01_index <= 2) {
            // 逐个将子元素的透明度设为1，实现文字逐个显示
            box01_p[box01_index].style.opacity = '1';
            box01_index++;
        }
        else {
            // 当文字全部显示完毕，清除定时器
            clearInterval(boxOneTimer);
        }
    }

    // 初始化数组 x_arr 和 y_arr 用于保存移动路径的坐标
    var x_arr = new Array();
    var y_arr = new Array();
    var x = y = m = 0;
    // 生成移动路径的坐标，共 800 个点
    for (var i = 0; i < 800; i++) {
        if (i >= 400) {
            // 下半部分路径
            x = m;
            y = 200 + Math.sqrt(40000 - Math.pow(x - 200, 2));
            m--;
        }
        else {
            // 上半部分路径
            x = m;
            y = 200 - Math.sqrt(40000 - Math.pow(x - 200, 2));
            m++;
        }
        // 将计算得到的坐标保存到数组中
        x_arr[i] = x - 60;
        y_arr[i] = parseInt(y) - 25;
    }

    // 获取三个移动元素的引用
    var gitMove = document.getElementById('github_a');
    var weiboMove = document.getElementById('weibo_a');
    var blogMove = document.getElementById('blog_a');

    // 初始化三个移动元素的计数器
    var g_num = 0;
    var w_num = 0;
    var b_num = 0;
    // 定义移动git元素的函数，用于实现元素的移动效果的定时器回调
    function movegit() {
        if (g_num <= 750) {
            gitMove.style.left = x_arr[g_num] + 'px';
            gitMove.style.top = y_arr[g_num] + 'px';
            g_num += 3;
        }
        else {
            // 当移动结束时，清除定时器
            clearInterval(gitTimer);
        }
    }

    // 定义移动微博元素的函数
    function moveweibo() {
        // 检查微博移动的次数是否小于等于 400 次
        if (w_num <= 400) {
            // 设置微博元素的 left 和 top 样式，使用 x_arr 和 y_arr 数组中的值
            weiboMove.style.left = x_arr[w_num] + 'px';
            weiboMove.style.top = y_arr[w_num] + 'px';
            // 每次移动 3 个步长
            w_num += 3;
        }
        else {
            // 当移动次数超过 400 次时，清除定时器，停止移动
            clearInterval(weiTimer);
        }
    }

    // 定义移动博客元素的函数
    function moveblog() {
        if (b_num <= 90) {
            // 设置博客元素的 left 和 top 样式，使用 x_arr 和 y_arr 数组中的值
            blogMove.style.left = x_arr[b_num] + 'px';
            blogMove.style.top = y_arr[b_num] + 'px';
            // 每次移动 3 个步长
            b_num += 3;
        }
        else {
            // 当移动次数超过 90 次时，清除定时器，停止移动
            clearInterval(blogTimer);
        }
    }

    // 调用 movegit 函数
    movegit();
    // 设置移动 git 元素的定时器，每毫秒执行一次 movegit 函数
    var gitTimer = setInterval(movegit, 1);
    // 调用 moveweibo 函数
    moveweibo();
    // 设置移动微博元素的定时器，每毫秒执行一次 moveweibo 函数
    var weiTimer = setInterval(moveweibo, 1);
    // 调用 moveblog 函数
    moveblog();
    // 设置移动博客元素的定时器，每毫秒执行一次 moveblog 函数
    var blogTimer = setInterval(moveblog, 1);
    // 设置定时器，每 800 毫秒执行一次 boxOne 函数
    var boxOneTimer = setInterval(boxOne, 800);
})();


// getElementById: 通过元素的 ID 获取对应的 DOM 元素
// getElementsByClassName: 通过元素的类名获取对应的 DOM 元素
// children: 获取指定元素的子元素集合
var f_btn = document.getElementById("float_btn").children;
var nav_ul = document.getElementById('nav_ul').children;
var wrapBox = document.getElementById("wrapBox");
var foot = document.getElementById("foot");
var arrow = document.getElementsByClassName("arrow_img");
var bg = document.getElementsByClassName("box_bg");
var boxs = document.getElementsByClassName("box");
var process = document.getElementById('bar_container').children;
var box02_text = document.getElementById('box02_text').children;
// 定义全局变量
var indexs = 0;
var box02_flag = false;
var box02_index = 0;
var box02_timer;


// 检查元素是否包含指定类名(cls)的函数
function hasClass(obj, cls) {

    // (\\s|^): 匹配类名的开头，确保它位于字符串的开头或者前面有一个空格
    // cls: 匹配传入的类名
    // (\\s|$): 匹配类名的结尾，确保它位于字符串的结尾或者后面有一个空格
    // match 方法，如果类名字符串中包含指定的类名，则返回匹配的结果；否则返回 null
    return obj.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

// 在元素上添加 'active' 类，前提是它没有这个类
function addActive() {
    for (var i = 0; i < process.length; i++) {
        if (!hasClass(process[i], 'active')) {
            process[i].className += ' active';
        }
    }
}


// 按顺序显示一组元素，每次调用将显示下一个元素，直到显示完所有元素
function boxTow() {
    if (box02_index >= 0 && box02_index < box02_text.length) {
        box02_text[box02_index].style.right = '0';
        box02_index++;
    }
    else {
        clearInterval(box02_timer);
    }
}

// 个简单的元素垂直移动的效果，根据目标高度和步长进行移动
function divMove(obj, overHeight, num) {
    if (obj.offsetTop >= overHeight) {
        var timer;
        timer = setInterval(function () {
            if ((obj.offsetTop - overHeight) <= num && (obj.offsetTop - overHeight) > 0) {
                obj.style.top = overHeight + 'px';
                clearInterval(timer);
            }
            else if ((obj.offsetTop - overHeight) > num) {
                obj.style.top = obj.offsetTop - num + 'px';
            }
        }, 10);
    }
    else if (obj.offsetTop < overHeight) {
        var timer;
        timer = setInterval(function () {
            if ((overHeight - obj.offsetTop) <= num && (overHeight - obj.offsetTop) > 0) {
                obj.style.top = overHeight + 'px';
                clearInterval(timer);
            }
            else if ((overHeight - obj.offsetTop) > num) {
                obj.style.top = obj.offsetTop + num + 'px';
            }
        }, 10);
    }
}

/**
 * btnChange - 控制页面切换和动画效果的函数
 *
 * @param {number} index - 切换到的页面索引
 * @param {boolean} flag - 是否启用动画效果
 * @param {number} speed - 动画速度
 */
function btnChange(index, flag, speed) {
    // 获取窗口高度和底部元素高度
    var height = window.innerHeight;
    var fh = foot.offsetHeight;

    // 清除所有按钮和导航项的样式
    for (var n = 0; n < f_btn.length; n++) {
        f_btn[n].style.background = "transparent";
    }

    for (var n = 0; n < nav_ul.length; n++) {
        nav_ul[n].style.color = '#000000';
    }

    // 判断是否启用动画效果
    if (flag) {
        if (index <= 0) {
            // 切换到第一页
            indexs = 0;
            f_btn[0].style.background = "#ffffff";
            nav_ul[0].style.color = 'red';
            // 执行页面切换动画
            divMove(wrapBox, 0, 20 * speed);
        }
        else if (index > 0 && index <= 3) {
            // 切换到第二到第四页
            f_btn[index].style.background = "#ffffff";
            nav_ul[index].style.color = 'red';
            // 执行页面切换动画
            divMove(wrapBox, -(height * indexs), 20 * speed);
        }
        else if (index == 4) {
            // 切换到最后一页
            indexs = 4;
            // 执行页面切换动画，显示最后一页
            divMove(wrapBox, (-height * 3 - fh), 20 * speed);
            nav_ul[index].style.color = 'red';
        }
        else {
            // 防止越界，保持在最后一页
            indexs = 4;
        }
    }
    else {
        // 不启用动画效果，直接设置样式
        if (index <= 0) {
            indexs = 0;
            f_btn[0].style.background = "#ffffff";
            nav_ul[0].style.color = 'red';
            wrapBox.style.top = "0";
        }
        else if (index > 0 && index <= 3) {
            f_btn[index].style.background = "#ffffff";
            nav_ul[index].style.color = 'red';
            wrapBox.style.top = (-height * indexs) + "px";
        }
        else if (index == 4) {
            indexs = 4;
            wrapBox.style.top = (-height * 3 - fh) + "px";
            nav_ul[index].style.color = 'red';
        }
        else {
            indexs = 4;
        }
    }

    // 设置特定条件下的标志和定时器
    if (indexs == 1 && !box02_flag) {
        box02_flag = true;
    }

    if (box02_flag) {
        // 设置定时器，在一定时间后执行addActive函数
        var timer = setTimeout(addActive, 1000);
        // 设置定时器，每500毫秒执行boxTow函数
        box02_timer = setInterval(boxTow, 500);
    }
}

// 给按钮组添加点击事件处理程序
for (var i = 0; i < f_btn.length; i++) {
    f_btn[i].indexs = i; // 为每个按钮添加索引属性
    f_btn[i].onclick = function () { // 按钮点击事件
        var speed = Math.abs(indexs - this.indexs); // 计算点击速度
        indexs = this.indexs; // 更新索引
        btnChange(indexs, true, speed); // 调用按钮切换函数
    }
}

// 给导航列表添加点击事件处理程序
for (var i = 0; i < nav_ul.length; i++) {
    nav_ul[i].indexs = i; // 为每个列表项添加索引属性
    nav_ul[i].onclick = function () { // 列表项点击事件
        var speed = Math.abs(indexs - this.indexs); // 计算点击速度
        indexs = this.indexs; // 更新索引
        btnChange(indexs, true, speed); // 调用按钮切换函数
    }
}

// 给箭头元素添加点击事件处理程序
for (var i = 0; i < arrow.length; i++) {
    arrow[i].indexs = i; // 为每个箭头添加索引属性
    arrow[i].onclick = function () { // 箭头点击事件
        indexs = this.indexs + 1; // 更新索引
        btnChange(indexs, true, 1); // 调用按钮切换函数
    }
}

var oB = true; // 初始化变量

// 鼠标滚轮事件处理程序
var scrollFunc = function (e) {
    var direct = 0; // 初始化方向变量
    e = e || window.event; // 兼容不同浏览器的事件对象

    if (e.wheelDelta) { // 鼠标滚轮事件处理（IE/Opera/Chrome）
        if (oB == true) { // 检查变量
            if (e.wheelDelta >= 120) { // 向上滚动
                oB = false; // 标记正在处理滚动事件
                indexs--; // 更新索引
                btnChange(indexs, true, 1); // 调用按钮切换函数
                setTimeout(function () {
                    oB = true; // 700ms后重置处理标记
                }, 700);
            } else if (e.wheelDelta <= -120) { // 向下滚动
                oB = false; // 标记正在处理滚动事件
                indexs++; // 更新索引
                btnChange(indexs, true, 1); // 调用按钮切换函数
                setTimeout(function () {
                    oB = true; // 700ms后重置处理标记
                }, 700);
            }
        }
    } else if (e.detail) { // 鼠标滚轮事件处理（Firefox）
        if (oB) { // 检查变量
            if (e.detail >= 3) { // 向下滚动
                oB = false; // 标记正在处理滚动事件
                indexs++; // 更新索引
                btnChange(indexs, true, 1); // 调用按钮切换函数
                setTimeout(function () {
                    oB = true; // 700ms后重置处理标记
                }, 700);
                console.log('ss'); // 打印消息到控制台
            } else if (e.detail <= -3) { // 向上滚动
                oB = false; // 标记正在处理滚动事件
                indexs--; // 更新索引
                btnChange(indexs, true, 1); // 调用按钮切换函数
                setTimeout(function () { oB = true; }, 700); // 700ms后重置处理标记
            }
        }
    }
}


// 添加鼠标滚轮事件监听（W3C标准）
if (document.addEventListener) {
    document.addEventListener('DOMMouseScroll', scrollFunc, false);
}

// 添加鼠标滚轮事件监听（IE/Opera/Chrome/Safari）
window.onmousewheel = document.onmousewheel = scrollFunc;

var iB = true; // 初始化变量，用于标记屏幕宽高比例
window.onresize = function () {
    // 动态调整根元素字体大小，适应不同屏幕宽度
    document.getElementsByTagName("html")[0].style.fontSize = document.documentElement.clientWidth / 20 + 'px';

    var h = window.innerHeight;
    var w = window.innerWidth;

    if (w / h >= 1920 / 1080) {
        iB = true; // 标记为宽屏
        imgChange(iB);
        btnChange(indexs);
    }
    else {
        iB = false; // 标记为窄屏
        imgChange(iB);
        btnChange(indexs, false);
    }
}

// 根据屏幕宽高比例调整图片和容器大小及位置
function imgChange(iB) {
    var h = window.innerHeight;
    var w = window.innerWidth;

    if (iB) { // 宽屏
        for (var i = 0; i < bg.length; i++) {
            bg[i].style.width = w + 'px';
            bg[i].style.height = w * (1080 / 1920) + 'px';
            bg[i].style.top = -(w * (1080 / 1920) - h) / 2 + 'px';
            boxs[i].style.width = w + 'px';
            boxs[i].style.height = h + 'px';
            bg[i].style.left = '0';
        }
    } else { // 窄屏
        for (var i = 0; i < bg.length; i++) {
            bg[i].style.height = h + 'px';
            bg[i].style.width = h * (1920 / 1080) + 'px';
            bg[i].style.left = -(h * (1920 / 1080) - w) / 2 + 'px';
            boxs[i].style.width = w + 'px';
            boxs[i].style.height = h + 'px';
            bg[i].style.top = '0';
        }
    }
}

// 获取时间轴元素和相关变量初始化
var e_li = document.getElementById('timeUl').children;
var li_times = 0;
var left_div = document.getElementById('left_div');
var right_div = document.getElementById('right_div');
const width_ = e_li[0].offsetWidth;// 每次移动宽度
var myWidth = 0;// 距离左边宽度

// 左侧按钮点击事件处理
left_div.onclick = function () {
    // 检查是否到达时间轴开始
    if (li_times == 0) {
        window.alert("已到达第一条");
        return;
    } else {
        // 向左滚动一个时间轴元素的宽度
        // console.log(myWidth, "a");
        // console.log(width_, "b");
        // console.log(Number(myWidth) + Number(e_li[0].offsetWidth), "a+b");
        document.getElementById('timeUl').style.left = Number(myWidth) + Number(width_) + 'px';
        myWidth += width_;
        li_times--;
    }
}

// 右侧按钮点击事件处理
right_div.onclick = function () {
    // 检查是否到达时间轴末尾
    if (li_times == (e_li.length - 2)) {
        window.alert("已到达最后一条");
        return;
    } else {
        // 向右滚动一个时间轴元素的宽度
        // console.log(myWidth, "a");
        // console.log(width_, "b");
        // console.log(Number(myWidth) - Number(e_li[0].offsetWidth), "a-b");
        document.getElementById('timeUl').style.left = Number(myWidth) - Number(width_) + 'px';
        myWidth -= width_;
        li_times++;
    }
}

var startY, endY;

// 添加触摸事件监听
window.addEventListener('touchstart', touchStart, false);
window.addEventListener('touchmove', touchMove, false);
window.addEventListener('touchend', touchEnd, false);

function touchStart(event) {
    // 获取触摸起始位置
    // var touch = event.originalEvent.targetTouches[0];
    // startY = touch.pageY;
    startY = event.touches[0].clientY;
}

function touchMove(event) {
    // 获取触摸移动位置
    // var touch = event.originalEvent.changedTouches[0];
    // endY = touch.pageY;
    endY = event.touches[0].clientY;
}

function touchEnd(event) {
    // 判断触摸方向并执行相应操作
    if (endY - startY > 100) {
        // 向下滑动，执行向上切换操作
        indexs = this.indexs - 1;
        btnChange(indexs, true, 1);
    } else if (startY - endY > 100) {
        // 向上滑动，执行向下切换操作
        indexs = this.indexs + 1;
        btnChange(indexs, true, 1);
    }
}
